package root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class GoHome {
    private static final Logger LOG = LoggerFactory.getLogger(GoHome.class);
    private static final String DATE_PROPERTIES_FILENAME = "vacation.properties";
    private static final String FORMAT_STRING = "format";
    private static final String WORKDAY_STRING = "workday";

    private static Properties properties;
    private static List<String> sortedPropertyKeys;

    public static boolean isWorkDay(String date) throws ParseException {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final Date day = dateFormat.parse(date);
        boolean workday = false;

        properties = getProperties();

        for (String key : sortedPropertyKeys) {
            String value = (String) properties.get(key);

            LOG.debug("{} <-> {}", key, value);

            if (key.contains(FORMAT_STRING)) {
                dateFormat.applyPattern(value);
                continue;
            }

            Date parsedDate = dateFormat.parse(value);

            if (key.contains(WORKDAY_STRING) && (isSameWeekDay(parsedDate, day) || isSameDay(parsedDate, day))) {
                LOG.debug("this day is a weekday");
                workday = true;
            }

            if (isSameDay(parsedDate, day) && !workday) {
                LOG.debug("this day is holiday");
                return false;
            }

        }

        return workday;
    }

    private static Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            try {
                properties.load(GoHome.class.getResourceAsStream(DATE_PROPERTIES_FILENAME));
            } catch (IOException e) {
                e.printStackTrace();
            }

            initSortedPropertyKeys();
        }

        return properties;
    }

    private static void initSortedPropertyKeys() {
        sortedPropertyKeys = new ArrayList<>();
        for (Object key : properties.keySet()) {
            sortedPropertyKeys.add((String) key);
        }

        Collections.sort(sortedPropertyKeys);
    }

    private static boolean isSameWeekDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);


        return cal1.get(Calendar.DAY_OF_WEEK) == cal2.get(Calendar.DAY_OF_WEEK);
    }

    private static boolean isSameDay(Date d1, Date d2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();

        cal1.setTime(d1);
        cal2.setTime(d2);

        return (cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH))
                && (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH));
    }

}
