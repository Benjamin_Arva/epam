package root.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Benjamin_Arva on 5/24/2016.
 */
public class RegexUtil {

    /**
     * The email is valid if:
     *  - It has a form of <name>@<domain>
     *  - The name must be at least 2 characters long
     *  •	The name may contains numbers and letters (upper and lower case) and the _ symbol.
     *      o	However:
     *          	the numbers can be only at the end of the name (example: Istvan_Kovacs2@bench.com)
     *          	the name must start with a letter
     *  •	The domain must be either bench.com, or a subdomain of bench.com (for example: sales.bench.com)
     *  •	The domain must be a lowercase string with dots as separator
     */
    private static final String EMAIL_REGEX = "^([a-zA-Z]+[_]*[a-zA-Z]*[0-9]*){2,}@([a-z]+\\.)*bench.com$";

    public static boolean isValidEmail(String email) throws IllegalArgumentException {
        if (email == null || email.isEmpty()) {
            throw new IllegalArgumentException("Email address shouldn't be null");
        }
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    /**
     * Create a java function which extracts identifiers, dates, names from the received URL Strings and return them to the caller.
     * Pattern:
     * http://www.cheaphotels.com/de{id}/Hotels-in-{name-of-city}/
     *
     * Sample:
     * http://www.cheaphotels.com/de56273/Hotels-in-London/
     *
     * @param url
     * @return
     * @throws IllegalArgumentException
     */
    public static String[] getHotelDataFromURL(String url) throws IllegalArgumentException {
        if (url == null || url.isEmpty()) {
            throw new IllegalAccessError("URL shouldn't be empty");
        }
        String[] extractedData = new String[2];

        Pattern pattern = Pattern.compile("/de(\\d+)/Hotels-in-(\\w+)");
        Matcher matcher = pattern.matcher(url);

        if (matcher.find()) {
            extractedData[0] = matcher.group(1);
            extractedData[1] = matcher.group(2);
        }

        return extractedData;
    }

    /**
     * Create a java function which extracts identifiers, dates, names from the received URL Strings and return them to the caller.
     *
     * Pattern:
     * http://airline-reservations.com/api/reservation/{resourceId}/from-{startDate}/to-{endDate}/
     *
     * Sample:
     * http://airline-reservations.com/api/reservation/178/from-2015-01-24/to-2015-01-25/
     * @param url
     * @return
     * @throws IllegalArgumentException
     */
    public static String[] getAirlineDataFromURL(String url) throws IllegalArgumentException {
        if (url == null || url.isEmpty()) {
            throw new IllegalArgumentException("URL shouldn't be empty");
        }
        String[] extractedData = new String[3];

        Pattern pattern = Pattern.compile("/(\\d+)/from-(\\d{4}-\\d{2}-\\d{2})/to-(\\d{4}-\\d{2}-\\d{2})");
        Matcher matcher = pattern.matcher(url);

        String asdd = "FSAJBFDIUOASBNFASIUPFHNAWPIU";

        if (matcher.find()) {
            extractedData[0] = matcher.group(1);
            extractedData[1] = matcher.group(2);
            extractedData[2] = matcher.group(3);
        }

        return extractedData;
    }
}
