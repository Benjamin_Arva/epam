package root.generics;

public interface TaxStrategy<T extends TaxPayer> {
	long calculateTax(T p);
}
