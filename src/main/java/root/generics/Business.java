package root.generics;

public class Business extends TaxPayer<Business> {
    private final int myNumberOfEmployees;

    public Business(long income, TaxStrategy taxStrategy, int numberOfEmployees) {
        super(income, taxStrategy);
        myNumberOfEmployees = numberOfEmployees;
    }

    public int getNumberOfEmployees() {
        return myNumberOfEmployees;
    }

    @Override
    protected Business getThis() {
        return this;
    }
}