package root.exceptions;

import java.util.List;

/**
 * Created by Benjamin_Arva on 5/23/2016.
 */
public interface IpAddressConverter {
    /**
     * <p>Converts an IP Address string to a numeric representation of the IP Address.</p>
     *
     * <p>The IP Address is converted the following way:</p>
     * <ul>
     *   <li>IP address (IPV4) is divided into 4 sub-blocks.</li>
     *   <li>Each sub-block has a different weight number each powered by 256.</li>
     * </ul>
     *
     * <p>
     *   IP Number is calculated based on the following formula:
     *   <pre>IP Number = 16777216*w + 65536*x + 256*y + z</pre>
     * </p>
     *
     * @see http://lite.ip2location.com/faqs
     *
     * @param ipAddress Must be a valid IP address (eg. 192.168.0.10)
     *
     * @throws IllegalArgumentException throwed if the given ip address is not valid
     */
    public long ipAddressToIPNumber(String ipAddress) throws IllegalArgumentException;

    /**
     * <p>Opens the specified comma separated file and returns its content in java objects.</p>
     *
     * <p>
     *   A comma separated file has the following format:
     *   <ul>
     *     <li>the first line is the header, it contains the name of the fields occurring in the data lines</li>
     *     <li>the data lines, which contain the data values in the order as it is specified in the header</li>
     *   </ul>
     *   The fields and values are separated with comma, end of the line is noted with a new line character.
     * </p>
     *
     * <pre>
     *   field1,field2,field3, ... fieldn
     *   value1,value2,value3, ... valuen
     *   ...
     *   value1,value2,value3, ... valuen
     * </pre>
     *
     * @param fileLocation  the location of the CSV file to process: the path and the name
     * @return a list, where each element represents a line of the CSV file. The elements are
     *         String arrays, which hold the fields of the actual line.
     */
    public List<String[]> readFile(final String fileLocation);
}
