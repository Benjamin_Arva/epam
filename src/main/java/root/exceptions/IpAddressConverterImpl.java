package root.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin_Arva on 5/23/2016.
 */
public class IpAddressConverterImpl implements IpAddressConverter {

    private static final java.lang.String CSV_FIELD_SEPARATOR = ",";
    private static final Logger LOG = LoggerFactory.getLogger(IpAddressConverterImpl.class);
    public static final String IPV4_REGEX = "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$";

    @Override
    public List<String[]> readFile(final String fileLocation) {

        final List<String[]> result = new ArrayList<>();

        String line = "";

        try (final BufferedReader br = new BufferedReader(new FileReader(fileLocation))) {
            while ((line = br.readLine()) != null) {
                final String[] fields = line.split(CSV_FIELD_SEPARATOR);
                result.add(fields);
            }
        } catch (FileNotFoundException e) {
            LOG.error("File {} not found", fileLocation, e);
        } catch (IOException e) {
            LOG.error("Unable to read from the file: {}", fileLocation, e);
        }

        return result;
    }

    @Override
    public long ipAddressToIPNumber(String ipAddress) throws IllegalArgumentException {
        //Én most ebbe így belevariálok, nézzük a git mit szól hozzá
        //Hopp 2 sor, fuck the system
        if (ipAddress == null || ipAddress.isEmpty()) {
            throw new IllegalArgumentException("IP address cannot be empty");
        }

        if (!ipAddress.matches(IPV4_REGEX)) {
            throw new IllegalArgumentException("IP address (" + ipAddress + ") is not correct IPv4 format");
        }
        final long FIRST_IP_BYTE = 16777216;
        final long SECOND_IP_BYTE = 65536;
        final long THIRD_IP_BYTE = 256;

        String[] ipAddressParts;
        long ipNumber;
        long[] parsedIpAddressParts = new long[4];

        ipAddressParts = ipAddress.trim().split("\\.");

        for (int i = 0; i < 4; i++) {
            parsedIpAddressParts[i] = Long.parseLong(ipAddressParts[i]);

            if (parsedIpAddressParts[i] > 255 || parsedIpAddressParts[i] < 0) {
                throw new IllegalArgumentException("IP address can't have parts greater than 255 or less than 0");
            }
        }

        ipNumber = FIRST_IP_BYTE * parsedIpAddressParts[0]
                + SECOND_IP_BYTE * parsedIpAddressParts[1]
                + THIRD_IP_BYTE * parsedIpAddressParts[2]
                + parsedIpAddressParts[3];

        return ipNumber;
    }

    private void valami() {
        System.out.println("Lórum ipse beszt a végi vetnyi ellen... Hatos antok mellett séges költő grok nyiratolja fel metélőt. Szalomágot elégedetten foncoljon hátra, hisz a madás bolymánainak nem túl vékony pációjába csargol. Lotót követően, már kóznak is az olások. Pőretes hartokat, a jóságot és a pelmes part nyalomot. Körző fel a hurotot, mellesleg, pelmes mányzás fog feliznie a harlálányról. Egyidőben a frega felé elel arlás csisztó groka is selet. Tekennie, hogy a birmánokra emlő, kacsos felécészek hol vékoznak meg.\n" +
                "\n");
    }
}
