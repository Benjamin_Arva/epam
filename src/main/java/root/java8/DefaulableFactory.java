package root.java8;

import java.util.function.Supplier;

/**
 * Created by Benjamin_Arva on 5/23/2016.
 */
public interface DefaulableFactory {
    static Defaulable create(Supplier<Defaulable> supplier) {
        return supplier.get();
    }
}
