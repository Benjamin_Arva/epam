package root.java8;

/**
 * Created by Benjamin_Arva on 5/23/2016.
 */
public class Application {

    /**
     * @return
     */
    public static Defaulable defaultInterface() {
        return DefaulableFactory.create(DefaulableImpl::new);
    }
}
