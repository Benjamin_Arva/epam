package root.java8;

/**
 * Created by Benjamin_Arva on 5/23/2016.
 */
public interface Defaulable {
    default String notRequired() {
        return "Not required to implement";
    }
}
