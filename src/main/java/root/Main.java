package root;

import root.regex.RegexUtil;

import java.io.File;
import java.util.Arrays;


/**
 * Created by Benjamin_Arva on 5/18/2016.
 */
public class Main {

    public static void main(String[] args) {
        String url1 = "http://www.cheaphotels.com/de56273/Hotels-in-London/";
        String url = "http://airline-reservations.com/api/reservation/178/from-2015-01-24/to-2015-01-25/";
        for (String s : RegexUtil.getAirlineDataFromURL(url)) {
//            System.out.println(s);
        }

        int i = 15_000;
        System.err.println(i);

    }

    private static void path(File input, int level) {
        if (!input.isDirectory()) {
            System.out.println(drawer(level) + " |- " + input.getName());
        } else {
            Arrays.asList(input.listFiles()).forEach(e -> {
                System.out.println(drawer(level) + " +- " + input.getName());
                path(e, level+1);
            });
        }
    }

    private static String drawer(int level) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < level * 4; i++) {
            sb.append(" ");
        }

        return sb.toString();
    }
}
