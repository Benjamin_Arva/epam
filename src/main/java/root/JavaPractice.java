package root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.text.Normalizer;
import java.util.regex.Pattern;

/**
 * Created by Benjamin_Arva on 5/20/2016.
 */
public class JavaPractice {

    private static final Logger LOG = LoggerFactory.getLogger(JavaPractice.class);

    public static int greatestCommonDivisor(int firstNumber, int secondNumber) {
        if (firstNumber == 0) {
            return secondNumber;
        }

        while (secondNumber != 0) {
            if (firstNumber > secondNumber) {
                firstNumber = firstNumber - secondNumber;
            } else {
                secondNumber = secondNumber - firstNumber;
            }
        }
        return firstNumber;
    }

    public static int greatestCommonDivisorRecursive(int firstNumber, int secondNumber) {
        return secondNumber == 0 ? firstNumber : greatestCommonDivisor(secondNumber, firstNumber % secondNumber);
    }

    /**
     * Write a function that capitalizes the first character of a given string.
     *
     * @param str
     * @return
     */
    public static String capitalize(String str) {
//        Character c =  Character.toUpperCase(str.charAt(0));
//        return str.replaceFirst(String.valueOf(str.charAt(0)), String.valueOf(c));
        return StringUtils.capitalize(str);
    }

    /**
     * Write a function that changes all Hungarian characters to the proper English ones.
     *
     * @param str
     * @return
     */
    public static String changeToEnglish(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    /**
     * Write a function that encodes a given string by iterating its characters, and XORs it with a given char.
     *
     * @param input
     * @param key
     * @return
     */
    public static String encode(String input, String key) {
        return base64Encode(xorWithKey(input.getBytes(), key.getBytes()));
//        return new String(xorWithKey(input.getBytes(), key.getBytes()));
    }

    /**
     * Write a decoder...
     *
     * @param input
     * @param key
     * @return
     */
    public static String decode(String input, String key) {
        return new String(xorWithKey(base64Decode(input), key.getBytes()));
//        return new String(xorWithKey(input.getBytes(), key.getBytes()));
    }

    private static byte[] xorWithKey(byte[] input, byte[] key) {
        byte[] out = new byte[input.length];
        for (int i = 0; i < input.length; i++) {
            out[i] = (byte) (input[i] ^ key[i % key.length]);
        }

        return out;
    }

    private static byte[] base64Decode(String input) {
        byte[] decodedBytes;
        try {
            BASE64Decoder d = new BASE64Decoder();
            decodedBytes = d.decodeBuffer(input);
        } catch (IOException e) {
            LOG.error("Error while decoding the input.");
            decodedBytes = input.getBytes();
        }
        return decodedBytes;
    }

    private static String base64Encode(byte[] bytes) {
        BASE64Encoder enc = new BASE64Encoder();
        return enc.encode(bytes).replaceAll("\\s", "");
    }
}
