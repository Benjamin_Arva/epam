import org.junit.Assert;
import org.junit.Test;
import root.regex.RegexUtil;

/**
 * Created by Benjamin_Arva on 5/24/2016.
 */
public class RegexUtilTester {

    @Test(expected = IllegalArgumentException.class)
    public void testNullEmail() {
        RegexUtil.isValidEmail(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyEmail() {
        RegexUtil.isValidEmail("");
    }

    @Test
    public void testRandomString() {
        Assert.assertFalse(RegexUtil.isValidEmail("sejhaj"));
    }

    @Test
    public void testRandomString2() {
        Assert.assertFalse(RegexUtil.isValidEmail("sejhaj@"));
    }

    @Test
    public void testRandomString3() {
        Assert.assertFalse(RegexUtil.isValidEmail("@sejhaj"));
    }

    @Test
    public void testShortUsername() {
        Assert.assertFalse(RegexUtil.isValidEmail("b@bench.com"));
    }

    @Test
    public void testValidEmail() {
        Assert.assertTrue(RegexUtil.isValidEmail("Bela@bench.com"));
    }

    @Test
    public void testValidEmail2() {
        Assert.assertTrue(RegexUtil.isValidEmail("Bela2@bench.com"));
    }

    @Test
    public void testEmailStartingWithNumber() {
        Assert.assertFalse(RegexUtil.isValidEmail("2Bela@bench.com"));
    }

    @Test
    public void testEmailWithNumbers() {
        Assert.assertTrue(RegexUtil.isValidEmail("Bela123123@bench.com"));
    }

    @Test
    public void testEmailWithUnderline() {
        Assert.assertTrue(RegexUtil.isValidEmail("Bela_Kiss@bench.com"));
    }

    @Test
    public void testEmailWithStarterUnderline() {
        Assert.assertFalse(RegexUtil.isValidEmail("_Bela@bench.com"));
    }

    @Test
    public void testFullValidEmail() {
        Assert.assertTrue(RegexUtil.isValidEmail("Istvan_Kovacs2@bench.com"));
    }

    @Test
    public void testInvalidDomain() {
        Assert.assertFalse(RegexUtil.isValidEmail("Istvan_Kovacs2@gmail.com"));
    }

    @Test
    public void testValidSubDomain() {
        Assert.assertTrue(RegexUtil.isValidEmail("Istvan@sales.bench.com"));
    }

    @Test
    public void testInvalidDomain2() {
        Assert.assertFalse(RegexUtil.isValidEmail("Istvan@Bench.com"));
    }

    @Test
    public void testInvalidDomain3() {
        Assert.assertFalse(RegexUtil.isValidEmail("Istvan@.bench.com"));
    }

    @Test
    public void testInvalidDomain4() {
        Assert.assertFalse(RegexUtil.isValidEmail("Istvan@sales_bench.com"));
    }

    @Test
    public void testInvalidDomain5() {
        Assert.assertFalse(RegexUtil.isValidEmail("Istvan@2bench.com"));
    }

    @Test
    public void testUrlDataExtraction() {
        String url = "http://www.cheaphotels.com/de56273/Hotels-in-London/";
        String[] expectedResult = {"56273", "London"};

        Assert.assertArrayEquals(expectedResult, RegexUtil.getHotelDataFromURL(url));
    }

    @Test
    public void testUrlDataExtraction2() {
        String url = "http://airline-reservations.com/api/reservation/178/from-2015-01-24/to-2015-01-25";
        String[] expectedResult = {"178", "2015-01-24", "2015-01-25"};

        Assert.assertArrayEquals(expectedResult, RegexUtil.getAirlineDataFromURL(url));
    }
}
